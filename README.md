# diceware-vue

This small web application allows to create passphrases using the “diceware”
method. Quoting [Wikipedia]:

> **Diceware** is a method for creating passphrases, passwords, and other
> cryptographic variables using ordinary dice as a hardware random number
> generator. For each word in the passphrase, five rolls of the dice are
> required. The numbers from 1 to 6 that come up in the rolls are assembled as a
> five-digit number, e.g. 43146. That number is then used to look up a word in a
> word list. In the English list 43146 corresponds to munch. By generating
> several words in sequence, a lengthy passphrase can be constructed. 

[Wikipedia]: https://en.wikipedia.org/wiki/Diceware

One goal of the application is visually explain the process to encourage users
to replicate it offline. Easily generating good passphrases is another. :D

## Usage

### Project setup

```sh
yarn install
```

### Compiles and hot-reloads for development

```sh
yarn serve
```

### Compiles and minifies for production

```sh
yarn build
```

### Lints and fixes files

```sh
yarn lint
```

## Technical details

The application is created using [Vue.js].

[Vue.js]: https://vuejs.org/

## Similar applications

 * <https://diceware.dmuth.org/> ([source](https://github.com/dmuth/diceware))
 * <https://www.rempe.us/diceware/> ([source](https://github.com/grempe/diceware))

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

A full copy of the GNU General Public License shoud be available in the
[LICENSE] file distributed with this program. If not, see
<https://www.gnu.org/licenses/>.

```
Copyright © 2020 Lunar <lunar@derivation.fr>
```

Icon “copy to clipboard” by Andrew from the Noun Project under Creative Commons BY: <https://thenounproject.com/term/copy-to-clipboard/1425165/>